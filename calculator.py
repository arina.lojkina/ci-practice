import math


class Calculator:
    def __init__(self):
        pass

    def add(self, x1, x2):
        return x1 + x2

    def multiply(self, x1, x2):
        return x1 * x2

    def subtract(self, x1, x2):
        return x1 - x2

    def divide(self, x1, x2):
        if x2 != 0:
            return x1 / x2

    def sin(self, x1):
        return math.sin(x1)

if __name__ == "__main__":
    calc = Calculator()
    print(calc.add(3, 4))
