import os
from setuptools import setup, find_packages


def read_description():
    abs_path = os.path.join(os.path.dirname(__file__), "README.rst")
    with open(abs_path) as f:
        return f.read()


setup(
    name='calculator',
    packages=find_packages(),
    include_package_data=True,
    test_suite='test',
    description='calculator proj',
    long_description=read_description(),
    python_requires=">=3.4"
)
