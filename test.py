import unittest
from calculator import Calculator


class TestCalculator(unittest.TestCase):
    def setUp(self):
        self.calculator = Calculator()

    def test_add(self):
        self.assertEqual(self.calculator.add(1, 1), 2)

    def test_subtract(self):
        self.assertEqual(self.calculator.subtract(5, 2), 3)

    def test_multiply(self):
        self.assertEqual(self.calculator.multiply(4, 3), 12)

    def test_divide(self):
        self.assertEqual(self.calculator.divide(6, 2), 3)

    def test_sin(self):
        self.assertEqual(self.calculator.sin(0), 0)


if __name__ == "__main__":
    unittest.main()
